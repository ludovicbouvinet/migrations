var width = window.innerWidth;
var height = window.innerHeight;

var points;

var plusLeft = 198;
var plusTop = 40;

// Je creer mon tableau d'objets avec pour chaque objet les informations concernant les étudiants
var DATA = new Array() ;

var positions = new Array();

var years = [1996,2000,2005,2010,2015,2016];

// Je fais une requette pour recuperer mes données dans mon fichier csv
d3.csv("datas/data.csv", function(data) {
    // Pour chaque donnée je remet l'objet dans mon tableau d'objet de données
    data.forEach(function(d) {
        DATA.push(d);
    })

    d3.select('.opening').on('click', function(){

var trans = d3.transition()
            .duration( 800 )
            .ease( d3.easeCubic );

        var current = d3.select(this);
        current
            .transition(trans)
            .attr('opacity', 0);

        current
            .transition(trans)
            .style('display', 'none');

        legend
            .transition(trans)
            .attr('opacity', 1);

        indications
            .transition(trans)
            .attr('opacity', 1);

        points
            .attr('opacity', 1);
        setSource( DATA, years[0] );

    });
    // Je reprend mon svg et je set mon image de fond pour ma carte
    var fullMap = svg
        .append('g')
        .attr('class', 'fullmap');

    var titles = svg
        .append('g')
        .attr('class', 'titles')
        .attr('y', 0)
        .attr('x', 0);

    var mainTitle = titles
        .append('text')
        .text('Migrations')
        .attr('y', plusTop + 20)
        .attr('x', 20);

    var subTitle = titles
        .append('text')
        .text('1996')
        .attr('y', 100 +15 )
        .attr('x', ((210 + 15) - 100) )
        .attr('class', 'subtitle');

    var leftArrow = svg
        .append('image')
        .attr('xlink:href', 'img/arrow.svg')
        .attr('class', 'arrow left-arrow cannot-use')
        .attr('x', -(plusTop + 50))
        .attr('y', 240)
        .attr('prev', -1);

    var rightArrow = svg
        .append('image')
        .attr('xlink:href', 'img/arrow.svg')
        .attr('class', 'arrow right-arrow can-use')
        .attr('x', (plusTop + 55))
        .attr('y', -260)
        .attr('next', 1);



    var clicable = d3.selectAll('.arrow')
    clicable.on('click', function(e){


        var that = d3.select(this);

        if(that.classed('can-use')){
            positions = new Array;

            if(that.classed('right-arrow')){

                yearID = that.attr('next');
                year = years[yearID]
                subTitle.text(year);
                that.attr('next', +yearID +1);

                var otherArrow = d3.select('.left-arrow');
                otherArrow.attr('prev', +otherArrow.attr('prev') +1 );

                if(otherArrow.attr('prev') != -1){
                    otherArrow.attr('class', 'arrow left-arrow can-use');
                }

                if((years[+yearID +1]) == undefined) {
                    that.attr('class', 'arrow right-arrow cannot-use');
                }

            }else if(that.classed('left-arrow')){
                yearID = that.attr('prev');
                year = years[yearID]
                subTitle.text(year);
                that.attr('prev', +yearID -1);

                var otherArrow = d3.select('.right-arrow');
                otherArrow.attr('next', +otherArrow.attr('next') -1 );

                if(otherArrow.attr('next') != 6){
                    otherArrow.attr('class', 'arrow right-arrow can-use');
                }

                if((years[+yearID -1]) == undefined) {
                    that.attr('class', 'arrow left-arrow cannot-use');
                }
            }

            setSource( DATA, year )
        }
    });

    var map = fullMap
        .append('image')
        .attr('xlink:href', 'img/France.svg')
        .attr('class', 'map')
        .attr('x', plusLeft);

    var indications = svg
        .append('g')
        .attr('class', 'indications')
        .attr('opacity', 0);

    var peoplesNbr = indications
        .append('text')
        .attr('class', 'nbr')
        .attr('total', 21)
        .text(DATA.length)
        .attr('y', (plusTop + 280 + 300))
        .attr('x', 20);

    var silhouette = indications
        .append('image')
        .attr('xlink:href', 'img/silhouette.svg')
        .attr('class', 'silhouette')
        .attr('y', (plusTop + 235 + 300))
        .attr('x', 70);

    var explanations = indications
        .append('text')
        .attr('class', 'explanations')
        .text(' de la formation ont été géolocalisés en France en 1996')
        .attr('y', (plusTop + 300 + 300))
        .attr('x', 20);

    var porcentage = indications
        .append('text')
        .attr('class', 'porcentage')
        .text('Soit '+ ( (+peoplesNbr.text() * 100) / +peoplesNbr.attr('total')) + '% des étudiants' )
        .attr('y', (plusTop + 280 + 300))
        .attr('x', 95);


    var legend = svg
        .append('g')
        .attr('class', "legend")
        .attr('opacity', 0);

    var minLegend = legend
        .append('g')
        .attr('class', 'min-legend');
    var maxLegend = legend
        .append('g')
        .attr('class', 'max-legend');

    var minCircle = minLegend
        .append('circle')
        .attr('r', 2.5)
        .attr('cx', 738 + 130)
        .attr('cy', 610)
        .attr('fill', 'white');

    var minTexte = minLegend
        .append('text')
        .text('1')
        .attr('x', 730 + 130)
        .attr('y', 640)
        .attr('class', 'explanations')
        .attr('fill', 'white');

    var minIllustration = minLegend
        .append('image')
        .attr('xlink:href', 'img/silhouette.svg')
        .attr('class', 'silhouette')
        .attr('y', 626)
        .attr('x', 735 + 130);


    var maxCircle = minLegend
        .append('circle')
        .attr('r', 30)
        .attr('cx', 738 + 52 + 130)
        .attr('cy', 610 - 30)
        .attr('fill', 'white');

    var maxTexte = minLegend
        .append('text')
        .text('21')
        .attr('x', 730 + 50 + 130)
        .attr('y', 640)
        .attr('class', 'explanations')
        .attr('fill', 'white');

    var maxIllustration = minLegend
        .append('image')
        .attr('xlink:href', 'img/silhouette.svg')
        .attr('class', 'silhouette')
        .attr('y', 626)
        .attr('x', 735 + 55 + 130);

    // Je créé mes points dans un 'g' et j'iterre sur chaque objet de mon tableau DATA
    // A partir de mon enter, mon d correspond a ma data actuel puisque je boucle.
    points = fullMap
        .append('g')
        .attr('x', plusLeft)
        .selectAll('circle')
        .data(DATA)
        .enter()
        .append('circle')
        .attr('class', 'point')
        .attr('cx', d => getDataByYear(1996, d, 'x') )
        .attr('cy', d => getDataByYear(1996, d, 'y') )
        .attr('r', 2.5)
        .attr('nextCoord', d => d['city1996'])
        .attr('prevCoord', 0)
        .attr('nbr', 1)
        .attr('viewed', true)
        .attr('currentR', 2.5)
        .attr('fill', 'white')
        .attr('opacity', 0);

d3.selectAll('.point').on('mouseover', function(){
    var current = d3.select(this);

    current.attr('fill', '#22fae0');
    peoplesNbr.text(current.attr('nbr'));
    porcentage.text('Soit '+ Math.round( (+peoplesNbr.text() * 100) / +peoplesNbr.attr('total')) + '% des étudiants' )
    explanations.text(' étaient présent ici cette année')

});

    d3.selectAll('.point').on('mouseleave', function(){
        d3.select(this).attr('fill', 'white')

        peoplesNbr.text(21);
        porcentage.text('Soit '+ Math.round( (+peoplesNbr.text() * 100) / +peoplesNbr.attr('total')) + '% des étudiants')
        explanations.text(' de la formation ont été géolocalisés en France cette année')
    });


});


var svg = d3.select('.container')
    .append('svg')
    .attr('viewBox', '0 0 1024 677')
    .attr('class', 'mainSVG');
    // .attr('webview');





function getDataByYear(year, data, axe) {

    // Pour chaque point que je change
    var name = 'city'+year;
    var position = data[name].split(',');

    position[0] = +position[0] + plusLeft;
    position[1] = +position[1] + plusTop;

    // Je definie ma position
    if(axe == 'x'){
        return position[0]
    }else if(axe == 'y'){
        positions.push(position);
        return position[1]
    }
}

function getOpacity(year, data){

    // Pour chaque point que je change
    var name = 'city'+year;
    var position = data[name].split(',');

    position[0] = +position[0] + plusLeft;
    position[1] = +position[1] + plusTop;

    var current = points.filter(function () {
        var cx = +this.attributes.cx.value
        var cy = +this.attributes.cy.value
        return cx === position[0] && cy === position[1]
    })

    var transit = d3.transition()
        .duration( 1000 )
        .ease( d3.easeCubic )

    current.attr('nbr', 1);
    positions.forEach(function(d) {

        var selected = points.filter(function () {
            var cx = +this.attributes.cx.value
            var cy = +this.attributes.cy.value
            var viewed = !!this.attributes.viewed.value
            return viewed && cx === d[0] && cy === d[1]
        })

        var distanceX = Math.abs(+d[0] - position[0]);
        var distanceY = Math.abs(d[1] - position[1]);
        var equal = distanceX + distanceY;

        if(distanceX < 40 && distanceY < 40 && equal != 0){

            selected
                .transition(transit)
                .attr('viewed', false)
                .attr('r', 0)
                .attr('nbr', 1);

            var oldR = current.attr('currentR');
            var newR = (+oldR * 0.9)  + 2.5;
            var oldNbr = +current.attr('nbr');
            current
                .attr('currentR', newR)
                .attr('nbr', (oldNbr +1) )
                .transition(transit)
                .attr('r', newR);

        }

    });

    return 1;
}

var t = d3.transition()
    .duration( 3000 )
    .ease( d3.easeCubic )


function setSource( data, year ) {
    this.points.data( data)
        .transition( t )
        .attr('cx', d => getDataByYear(year, d, 'x') )
        .attr('cy', d => getDataByYear(year, d, 'y') )
        .attr('r', 2.5)
        .attr('nbr', 1)
        .attr('viewed', true)
        .attr('currentR', 2.5)
        .on("end", d => getOpacity(year, d));
        // .attr('opacity', d => getOpacity(year, d));
}